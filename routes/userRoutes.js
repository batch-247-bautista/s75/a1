const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");

// Check the email
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Register a user
router.post("/register", (req, res) => {
  const userData = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    mobileNo: req.body.mobileNo,
    password: req.body.password
  };

  userController
    .registerUser(userData)
    .then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



// Route for purchasing an order
router.post("/checkout", auth.verify, (req, res) => {
  const data = {
    userId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId
  };
  userController.order(data)
    .then(result => res.send(result))
    .catch(error => res.status(500).json({ error: error.message }));
});



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
